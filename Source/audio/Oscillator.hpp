//
//  Oscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 29/11/2016.
//
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

/**
 Base class for all oscillators
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     Oscillator constructor
     */
    Oscillator();
    
    /**
     Oscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     @param float freq is the new frequency value of the oscillator
     */
    void setFrequency (float freq);
    float getFrequency () {return frequency;}
    
    /**
     sets frequency using a midi note number
     @param int noteNum is the new MIDI note number between 0 and 127
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     @param float amp is the new amplitude between 0 and 1
     */
    void setAmplitude (float amp);
    float getAmplitude () {return amplitude;}
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     @param float sr is the new sample rate usually 441000(Hz)
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     @return retunr the next sample
     */
    float nextSample();
    
    /**
     function that provides the execution of the waveshape
     @param const float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position (when it's overriden as this is a pure function. Usually between -1 and +1.
     */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};


#endif /* Oscillator_hpp */
