//
//  Voices.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 03/12/2016.
//
//

#ifndef Voices_hpp
#define Voices_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.hpp"
#include "SquareOscillator.hpp"
#include "SawOscillator.hpp"
#include "AttackRelease.hpp"

/**
 Voices class - Voices are created to implement polyphonic synthesiser
 */
class Voices
{
public:
    /**
     Constructor - initialise everything
     */
    Voices ();
    
    /**
     Destructor
     */
    ~Voices();
    
    void startNote();
    void stopNote();
    void setWaveform(int newWaveform);
    void setFrequency(float newFrequency);
    float getFrequency();
    void setAmplitude(float newAmplitude);
    float getAmplitude();
    void setSampleRate(int newSampleRate);
    float nextSample();
    void startAttack () { attackRelease.resetAttackCounter(); }
    void startRelease () { attackRelease.resetReleaseCounter(); }
    
private:
    Oscillator* osc;
    SinOscillator oscSin;
    SquareOscillator oscSquare;
    SawtoothOscillator oscSaw;
    AttackRelease attackRelease;


#endif /* Voices_hpp */
