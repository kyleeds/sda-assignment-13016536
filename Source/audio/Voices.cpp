//
//  Voices.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 03/12/2016.
//
//

#include "Voices.hpp"

Voices::Voices()
{
    
}

Voices::~Voices()
{
    
}

void Voices::startNote()
{
    attackRelease.resetAttackCounter();
}

void Voices::stopNote()
{
    attackRelease.resetReleaseCounter();
}

float Voices::nextSample()
{
    return osc->nextSample() * attackRelease.attack() * attackRelease.release();
    if (attackRelease.release() == 0.f) {
        osc->setFrequency(0.f);
        osc->setAmplitude(0.f);
    }
}

void Voices::setFrequency(float newFrequency)
{
    osc->setFrequency(newFrequency);
}
float Voices::getFrequency()
{
    return osc->getFrequency();
}

void Voices::setAmplitude(float newAmplitude)
{
    osc->setAmplitude(newAmplitude);
}
float Voices::getAmplitude()
{
    return osc->getAmplitude();
}

void Voices::setWaveform(int newWaveform)
{
    if (newWaveform == 1) {
        osc = &oscSin;
    }
    else if (newWaveform == 2)
    {
        osc = &oscSquare;
    }
    else
    {
        osc = &oscSaw;
    }
}
void Voices::setSampleRate(int newSampleRate)
{
    oscSin.setSampleRate (newSampleRate);
    oscSquare.setSampleRate (newSampleRate);
    oscSaw.setSampleRate (newSampleRate);
    attackRelease.setSampleRate(newSampleRate);
}