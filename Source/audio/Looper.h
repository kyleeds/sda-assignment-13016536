//
//  Looper.h
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#ifndef H_Looper
#define H_Looper

#include "../JuceLibraryCode/JuceHeader.h"
#include "Metronome.hpp"
#include "SinOscillator.hpp"
#include "SquareOscillator.hpp"
#include "SawOscillator.hpp"
#include "AttackRelease.hpp"
#include "Voices.hpp"

/**
 Simple audio looper class - loops an audio buffer and records its input into the buffer.
 Also produces a click 4 times each loop.
 */
class Looper
{
public:
    /**
     Constructor - initialise everything 
     */
    Looper (Metronome& metronome_);
    
    /**
     Destructor
     */
    ~Looper();
    
    /**
     Starts or stops playback of the looper
     */
    void setPlayState (bool newState);
    
    /**
     Gets the current playback state of the looper
     */
    bool getPlayState() const;
    
    /**
     Sets/unsets the record state of the looper
     */
    void setRecordState (bool newState);
    
    /**
     Gets the current record state of the looper
     */
    bool getRecordState() const;
    
    /**
     Sets/unsets the input state of the looper
     */
    void setInputState (bool newState);
    
    /**
     Gets the current input state of the looper
     */
    bool getInputState() const;
    
    /**
     Processes the audio sample by sample.
     */
    float processSample (float input);
    
    /**
     Saves the loop
     */
    void save ();
    
    /**
     Loads a new loop
     */
    void load ();
    
    /**
     Deletes current loop
     */
    void deleteLoop ();
    
    /**
     Sets the waveform state of the looper
     */
    void setWaveform (int newWaveformId);
    
    void setFrequency (int newFrequency);
    void setAmplitude (float newAmplitude);
    void setOverallGain (float newGain);
    void setSampleRate (int newSampleRate);
    bool getMetronomeClick ();
    void startAttack () { attackRelease.resetAttackCounter(); }
    void startRelease () { attackRelease.resetReleaseCounter(); }

private:  
    //Shared data
    Atomic<int> recordState;
    Atomic<int> playState;
    Atomic<int> inputState;
    Atomic<int> currentWaveform;
    
    AudioSampleBuffer audioSampleBuffer;
    
    Metronome& metronome;
    bool metronomeClick;
    float amplitude;
    float* audioSample;
    
    OwnedArray<Voices> voices;
    int voiceTracker = 0;
    
    Oscillator* osc;
    SinOscillator oscSin;
    SquareOscillator oscSquare;
    SawtoothOscillator oscSaw;
    AttackRelease attackRelease;
};

#endif /* H_Looper */
