//
//  MetronomeGui.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 24/11/2016.
//
//

#include "MetronomeGui.hpp"

MetronomeGui::MetronomeGui(Metronome& metronome_) : metronome (metronome_)
{
    metronomeButton.setButtonText ("Turn Metronome On");
    metronomeButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    metronomeButton.setColour(TextButton::buttonColourId, Colours::grey);
    metronomeButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (metronomeButton);
    metronomeButton.addListener(this);
    
    bpmSlider.setSliderStyle(Slider::LinearHorizontal);
    bpmSlider.setRange(20.f, 300.f);
    bpmSlider.setValue(120.f);
    addAndMakeVisible(bpmSlider);
    bpmSlider.addListener(this);
}
void MetronomeGui::buttonClicked (Button* button)
{
    if (button == &metronomeButton)
    {
        metronome.setMetronomeState (!metronome.getMetronomeState());
        metronomeButton.setToggleState (metronome.getMetronomeState(), dontSendNotification);
        if (metronome.getMetronomeState())
        {
            metronomeButton.setButtonText ("Turn Metronome Off");
        }
        else
        {
            metronomeButton.setButtonText ("Turn Metronome On");
        }
    }
}

void MetronomeGui::sliderValueChanged(Slider *slider)
{
    if (slider == &bpmSlider) {
        metronome.setBpm(bpmSlider.getValue());
    }
}

void MetronomeGui::resized()
{
    metronomeButton.setBounds(0, 0, getWidth()/4, getHeight());
    bpmSlider.setBounds(getWidth()/4, 0, 3*getWidth()/4, getHeight());
}