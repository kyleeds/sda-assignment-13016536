/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_)
 :  audio (audio_),
    looperGui (audio_.getLooper1()),
    looperGui2 (audio_.getLooper2()),
    metronomeGui(audio.getMetronome())
{
    setSize (700, 600);
    addAndMakeVisible (looperGui);
    addAndMakeVisible (looperGui2);
    addAndMakeVisible(metronomeGui);
    
    startStopButton.setButtonText ("Start");
    startStopButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    startStopButton.setClickingTogglesState (true);
    startStopButton.setColour(TextButton::buttonColourId, Colours::grey);
    startStopButton.setColour(TextButton::buttonOnColourId, Colours::green);
    addAndMakeVisible (startStopButton);
    startStopButton.addListener(this);
    
    inputButton.setButtonText ("Use Synth");
    inputButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    inputButton.setClickingTogglesState (true);
    inputButton.setColour(TextButton::buttonColourId, Colours::purple);
    inputButton.setColour(TextButton::buttonOnColourId, Colours::lightblue);
    addAndMakeVisible (inputButton);
    inputButton.addListener(this);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    startStopButton.setBounds(0, 0, getWidth()/5, 80);
    inputButton.setBounds(getWidth()/5, 0, getWidth()/5, 80);
    
    metronomeGui.setBounds(2*getWidth()/5, 0, 3*getWidth()/5, 80);
    
    looperGui.setBounds(0, 90, getWidth(), 80);
    looperGui2.setBounds(0, getHeight()/2, getWidth(), 80);
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &startStopButton)
    {
        if (startStopButton.getToggleState() == true)
            startStopButton.setButtonText ("Stop");
        else
            startStopButton.setButtonText ("Play");
        
        audio.setPlayState (startStopButton.getToggleState());
    }
    else if (button == &inputButton)
    {
        if (inputButton.getToggleState() == true)
        {
            inputButton.setButtonText ("Use Mic");
        }
        else
        {
            inputButton.setButtonText ("Use Synth");
        }
        audio.setInputState (inputButton.getToggleState());
//        audio.setInputState (!audio.getInputState());
//        inputButton.setToggleState(audio.getInputState(), dontSendNotification);
    }
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

