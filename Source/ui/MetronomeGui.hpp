//
//  MetronomeGui.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 24/11/2016.
//
//

#ifndef MetronomeGui_hpp
#define MetronomeGui_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include "Metronome.hpp"

/**
 Gui for the Metronome class
 */
class MetronomeGui :    public Component,
                        public Button::Listener,
                        public Slider::Listener
{
public:
    /**
     constructor - receives a reference to a Looper object to control
     */
    MetronomeGui(Metronome& metronome_);
    
    //Button Listener
    void buttonClicked (Button* button) override;
    
    void sliderValueChanged (Slider* slider) override;
    
    //Component
    void resized() override;
private:
    Metronome& metronome;                 //reference to a looper object
    TextButton metronomeButton;
    Slider bpmSlider;
};


#endif /* MetronomeGui_hpp */
